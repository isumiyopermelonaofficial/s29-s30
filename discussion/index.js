// Always use express for server site projects
const express = require('express');
const application = express();
const port = 4000;

application.listen(port,  () => console.log(`Server is running on ${port}`));
